from http import client
from flask import Flask, request, jsonify
from twilio.base.exceptions import TwilioRestException
from twilio.rest import Client
from twilio.twiml.messaging_response import MessagingResponse
from newoperations import NewOperations
from newques import new_questions_set

app = Flask(__name__)

# test ids
test_account_sid = 'AC2b7536c6b23aa48f1b1986fa86106f85'
test_auth_token = '9189c9f07b651d76e3a767fcb2f1f571'

# production ids
account_sid = "AC7970bec0869ad195b28730608661a23b"
auth_token = "ec48cee946f71ea10046a515d0cd3337"
client = Client(account_sid, auth_token)

previouslyAskedQuestionsStack = []
operations = NewOperations()

def detectAndPerformAppropriateOperationForSingleInput(previouslyAskedQuestionData, answer):
    actionCode = previouslyAskedQuestionData[1][0]
    nextItem = previouslyAskedQuestionData[2][0]
    return operations.takeAppropriateActions(actionCode, previouslyAskedQuestionsStack, nextItem)

def detectAndPerformAppropriateOperation(previouslyAskedQuestionData, answer):
    answer = int(answer)-1
    actionCode = previouslyAskedQuestionData[1][answer]
    nextItem = previouslyAskedQuestionData[2][answer]
    return operations.takeAppropriateActions(actionCode, previouslyAskedQuestionsStack, nextItem)

def validateTakeActionAndRespond(answer):
    if answer == 0:
        previouslyAskedQuestionsStack.clear()
    if not previouslyAskedQuestionsStack:
        previouslyAskedQuestionsStack.append(new_questions_set[1])
        return operations.generateLastContent(previouslyAskedQuestionsStack)

    lastQuestionData = previouslyAskedQuestionsStack[-1]
    contentLength = len(lastQuestionData[0])

    #   validate response only for the multiple choice questions
    if(contentLength > 1):
        if(operations.validateResponse(answer, contentLength)):
            return detectAndPerformAppropriateOperation(lastQuestionData, answer)
        else:
            raise Exception(
                "Wrong input Try Again" + operations.generateLastContent(previouslyAskedQuestionsStack))
    else:
        return detectAndPerformAppropriateOperationForSingleInput(lastQuestionData, answer)

@app.route('/message', methods=['POST'])
def chatbot():
    try:
        answer = request.form.get('data')
        response = validateTakeActionAndRespond(answer)
        message = client.messages.create(
            to="whatsapp:+16047211467",
            from_="whatsapp:+14155238886",
            body=response
        )
        return jsonify(message.body)
    except TwilioRestException as _error:
        return jsonify(_error.msg)
    except Exception as e:
        return jsonify(str(e))

@app.route('/call', methods=['GET'])
def call():
    try:
        
        call = client.calls.create(
            url='http://demo.twilio.com/docs/voice.xml',
            to='+917398435583',
            from_='+16047211467'
        )
        return jsonify(call)
    except TwilioRestException as _error:
        return jsonify(_error.msg)
    except Exception as e:
        return jsonify(str(e))

if __name__ == '__main__':
    app.run(host='127.0.0.1', port=80, debug=False)
