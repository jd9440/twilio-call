new_options = {
    
    0: "Go Back",

    1: "Registration",
    2: "Our Services",
    3: "Our Clients",
    4: "Products Information",
    5: "Sales Information",
    6: "Purchase Information",
    7: "Customer Support",

    8: "Enter Your First Name",
    9: "Enter Your Last Name",
    10: "Enter Your Email Address",
    11: "Enter Your Phone Number",
    12: "Enter the verification code from email",
    
    13: "See Web Development Clients",
    14: "See App Development Clients",
    15: "See Other Clients",

    16: "Enter Product ID",

    17: "Enter email",
    18: "Enter Password",
    19: "Enter Sale Order ID",

    20: "Enter email",
    21: "Enter Password",
    22: "Enter Purchase Order ID",

    23: "Register complaint",
    24: "Talk to customer executive",
    25: "Complaint regarding product quality",
    26: "Delivery Issues",
    27: "Other",

}   
