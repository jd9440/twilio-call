# Add operation ID in individual list items to perforrm different operations
# tuple structure, instruction_id, list of options_ids, list of operations associated with option_ids,
questions_set = {
    1: (
        # content / options
        [1, 2, 3, 4, 5],

        # action codes
        [101, 102, 103, 104, 105],

        # related options/ questions
        [6, 11, 13, 14, 15]
    ),
    6: (
        [6],
        [106],
        [7]
    ),
    7: (
        [7],
        [107],
        [8]
    ),
    8: (
        [8],
        [108],
        [9]
    ),
    9: (
        [9],
        [109],
        [10]
    ),
    11: (
        [11],
        [110],
        [12]
    ),
    12: (
        [12],
        [111],
        []
    ),
    13: (
        [13],
        [112],
        []
    ),
    14: (
        [14],
        [113],
        []
    ),
    15: (
        [15],
        [114],
        []
    ),
}
