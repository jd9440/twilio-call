from re import match
from questions import questions_set
from options import options

class Operations:

    def validateResponse(self, response,length):
        response = int(response)
        return response in range(1,length+1)

    def generateLastContent(self, previouslyAskedQuestionsStack):
        firstElement = previouslyAskedQuestionsStack[-1]
        content = ""
        for index in firstElement[0]:
            content += str(index) + ". " + str(options[index]) + "\n"
        return content

    def takeAppropriateActions(self, actionCode, previouslyAskedQuestionsStack, nextItem):
        if actionCode==101:
            previouslyAskedQuestionsStack.append(questions_set[nextItem])
            return self.generateLastContent(previouslyAskedQuestionsStack)
            
        if actionCode==102:
            print("perform login")
            previouslyAskedQuestionsStack.append(questions_set[nextItem])
            return self.generateLastContent(previouslyAskedQuestionsStack)
            
        if actionCode==103:
            print("perform fetch personal info")
            previouslyAskedQuestionsStack.append(questions_set[nextItem])
            return self.generateLastContent(previouslyAskedQuestionsStack)

        if actionCode==104:
            print("perform sales personal info")
            previouslyAskedQuestionsStack.append(questions_set[nextItem])
            return self.generateLastContent(previouslyAskedQuestionsStack)

        if actionCode==105:
            print("perform purchase personal info")
            previouslyAskedQuestionsStack.append(questions_set[nextItem])
            return self.generateLastContent(previouslyAskedQuestionsStack)

        if actionCode==106:
            print("ask for first name")
            previouslyAskedQuestionsStack.append(questions_set[nextItem])
            return self.generateLastContent(previouslyAskedQuestionsStack)

        if actionCode==107:
            print("ask for last name")
            previouslyAskedQuestionsStack.append(questions_set[nextItem])
            return self.generateLastContent(previouslyAskedQuestionsStack)
            
        if actionCode==108:
            print("ask for email")
            previouslyAskedQuestionsStack.append(questions_set[nextItem])
            return self.generateLastContent(previouslyAskedQuestionsStack)

        if actionCode==109:
            print("ask for phone number")
            previouslyAskedQuestionsStack.append(questions_set[nextItem])
            return self.generateLastContent(previouslyAskedQuestionsStack)
            
        if actionCode==110:
            print("show entered info")
            previouslyAskedQuestionsStack.append(questions_set[nextItem])
            return self.generateLastContent(previouslyAskedQuestionsStack)
            
        if actionCode==111:
            print("ask for user name")
            previouslyAskedQuestionsStack.append(questions_set[nextItem])
            return self.generateLastContent(previouslyAskedQuestionsStack)
            
        if actionCode==112:
            print("ask for word")
            previouslyAskedQuestionsStack.append(questions_set[nextItem])
            return self.generateLastContent(previouslyAskedQuestionsStack)
            
        if actionCode==113:
            print("ask for email/phone")
            previouslyAskedQuestionsStack.append(questions_set[nextItem])
            return self.generateLastContent(previouslyAskedQuestionsStack)
            
        if actionCode==114:
            print("ask for sale id")
            previouslyAskedQuestionsStack.append(questions_set[nextItem])
            return self.generateLastContent(previouslyAskedQuestionsStack)
            
    
    # def performValidation(self, actionCode):
    #     if actionCode==101:
            
            
    #     if actionCode==102:
            
            
    #     if actionCode==103:
            
            
    #     if actionCode==104:
            
            
    #     if actionCode==105:
            
            
    #     if actionCode==106:
            
            
    #     if actionCode==107:
            
            
    #     if actionCode==108:
            
    #     if actionCode==109:
            
            
    #     if actionCode==110:
            
            
    #     if actionCode==111:
            
            
    #     if actionCode==112:
            
            
    #     if actionCode==113:
            
            
    #     if actionCode==114:
            
            