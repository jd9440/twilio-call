from re import match
from newques import new_questions_set
from moreoptions import new_options

class NewOperations:

    def validateResponse(self, response,length):
        response = int(response)
        return response in range(1,length+1)

    def generateLastContent(self, previouslyAskedQuestionsStack):
        firstElement = previouslyAskedQuestionsStack[-1]
        content = ""
        for index in firstElement[0]:
            content += str(index) + ". " + str(new_options[index]) + "\n"
        return content

    def takeAppropriateActions(self, actionCode, previouslyAskedQuestionsStack, nextItem):
        if actionCode==101:
            print("Register")
            previouslyAskedQuestionsStack.append(new_questions_set[nextItem])
            return self.generateLastContent(previouslyAskedQuestionsStack)
            
        if actionCode==102:
            print("show services")
            previouslyAskedQuestionsStack.append(new_questions_set[nextItem])
            return self.generateLastContent(previouslyAskedQuestionsStack)
            
        if actionCode==103:
            print("show clients")
            previouslyAskedQuestionsStack.append(new_questions_set[nextItem])
            return self.generateLastContent(previouslyAskedQuestionsStack)

        if actionCode==104:
            print("show product info")
            previouslyAskedQuestionsStack.append(new_questions_set[nextItem])
            return self.generateLastContent(previouslyAskedQuestionsStack)

        if actionCode==105:
            print("show sales info")
            previouslyAskedQuestionsStack.append(new_questions_set[nextItem])
            return self.generateLastContent(previouslyAskedQuestionsStack)

        if actionCode==106:
            print("show purchase info")
            previouslyAskedQuestionsStack.append(new_questions_set[nextItem])
            return self.generateLastContent(previouslyAskedQuestionsStack)

        if actionCode==107:
            print("open customer support")
            previouslyAskedQuestionsStack.append(new_questions_set[nextItem])
            return self.generateLastContent(previouslyAskedQuestionsStack)
            
        if actionCode==108:
            print("get first name")
            previouslyAskedQuestionsStack.append(new_questions_set[nextItem])
            return self.generateLastContent(previouslyAskedQuestionsStack)
            
        if actionCode==109:
            print("get last name")
            previouslyAskedQuestionsStack.append(new_questions_set[nextItem])
            return self.generateLastContent(previouslyAskedQuestionsStack)
            
        if actionCode==110:
            print("ask for email")
            previouslyAskedQuestionsStack.append(new_questions_set[nextItem])
            return self.generateLastContent(previouslyAskedQuestionsStack)

        if actionCode==111:
            print("ask for phone number")
            previouslyAskedQuestionsStack.append(new_questions_set[nextItem])
            return self.generateLastContent(previouslyAskedQuestionsStack)
            
        if actionCode==112:
            print("get verification code")
            previouslyAskedQuestionsStack.append(new_questions_set[nextItem])
            return self.generateLastContent(previouslyAskedQuestionsStack)
            
        if actionCode==113:
            print("web dev clients")
            previouslyAskedQuestionsStack.append(new_questions_set[nextItem])
            return self.generateLastContent(previouslyAskedQuestionsStack)
            
        if actionCode==114:
            print("app dev clients")
            previouslyAskedQuestionsStack.append(new_questions_set[nextItem])
            return self.generateLastContent(previouslyAskedQuestionsStack)
            
        if actionCode==115:
            print("other clients")
            previouslyAskedQuestionsStack.append(new_questions_set[nextItem])
            return self.generateLastContent(previouslyAskedQuestionsStack)
            
        if actionCode==116:
            print("get product Id")
            previouslyAskedQuestionsStack.append(new_questions_set[nextItem])
            return self.generateLastContent(previouslyAskedQuestionsStack)
            
        if actionCode==117:
            print("get user email")
            previouslyAskedQuestionsStack.append(new_questions_set[nextItem])
            return self.generateLastContent(previouslyAskedQuestionsStack)
            
        if actionCode==118:
            print("get password")
            previouslyAskedQuestionsStack.append(new_questions_set[nextItem])
            return self.generateLastContent(previouslyAskedQuestionsStack)
            
        if actionCode==119:
            print("get sales Id")
            previouslyAskedQuestionsStack.append(new_questions_set[nextItem])
            return self.generateLastContent(previouslyAskedQuestionsStack)

        if actionCode==120:
            print("get user email")
            previouslyAskedQuestionsStack.append(new_questions_set[nextItem])
            return self.generateLastContent(previouslyAskedQuestionsStack)
            
        if actionCode==121:
            print("get password")
            previouslyAskedQuestionsStack.append(new_questions_set[nextItem])
            return self.generateLastContent(previouslyAskedQuestionsStack)
            
        if actionCode==122:
            print("get purchase Id")
            previouslyAskedQuestionsStack.append(new_questions_set[nextItem])
            return self.generateLastContent(previouslyAskedQuestionsStack)
        
        if actionCode==123:
            print("register complaint")
            previouslyAskedQuestionsStack.append(new_questions_set[nextItem])
            return self.generateLastContent(previouslyAskedQuestionsStack)

        if actionCode==124:
            print("talk to customer care")
            previouslyAskedQuestionsStack.append(new_questions_set[nextItem])
            return self.generateLastContent(previouslyAskedQuestionsStack)
            
        if actionCode==125:
            print("product complaint")
            previouslyAskedQuestionsStack.append(new_questions_set[nextItem])
            return self.generateLastContent(previouslyAskedQuestionsStack)
            
        if actionCode==126:
            print("delivery complaint")
            previouslyAskedQuestionsStack.append(new_questions_set[nextItem])
            return self.generateLastContent(previouslyAskedQuestionsStack)

        if actionCode==127:
            print("other complaint")
            previouslyAskedQuestionsStack.append(new_questions_set[nextItem])
            return self.generateLastContent(previouslyAskedQuestionsStack)
            
        # if actionCode==128:
        #     print("go back to previous menu")
        #     previouslyAskedQuestionsStack.clear()
        #     print("performing action 128")
        #     return self.generateLastContent(previouslyAskedQuestionsStack)
    
    # def performValidation(self, actionCode):
    #     if actionCode==101:
            
            
    #     if actionCode==102:
            
            
    #     if actionCode==103:
            
            
    #     if actionCode==104:
            
            
    #     if actionCode==105:
            
            
    #     if actionCode==106:
            
            
    #     if actionCode==107:
            
            
    #     if actionCode==108:
            
    #     if actionCode==109:
            
            
    #     if actionCode==110:
            
            
    #     if actionCode==111:
            
            
    #     if actionCode==112:
            
            
    #     if actionCode==113:
            
            
    #     if actionCode==114:
            
            