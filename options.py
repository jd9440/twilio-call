options = {
    
    1: "Registration",
    2: "Log In",
    3: "Get Profile Information",
    4: "Fetch Sale Information",
    5: "Fetch Purchase Information",

    6: "Enter Your First Name",
    7: "Enter Your Last Name",
    8: "Enter Your Email Address",
    9: "Enter Your Phone Number",
    10: "Press 1 to Confirm Information And Done",
    
    11: "Enter Username",
    12: "Enter Password",

    13: "Enter PhoneNumber / Email Address",

    14: "Enter Sale Order ID",

    15: "Enter Purchase Order ID",
}   
